import React, {Component} from 'react'
import { Route, NativeRouter, Switch, Redirect} from 'react-router-native'
import Login from './Login'
import Profile from './Profile'
import List from './List'
import EditProfile from './EditProfile'
import AddProduct from './AddProduct';
import Product from './Product';

class Router extends Component {
    render() {
        return (
           <NativeRouter>
               <Switch>
                   <Route exact path="/login" component={Login} />
                   <Route exact path="/profile" component={Profile} />
                   <Route exact path="/list" component={List}/>
                   <Route exact path="/editprofile" component={EditProfile}/>
                   <Route exact path="/addproduct" component={AddProduct}/>
                   <Route exact path="/product" component={Product}/>
                   <Redirect to="/login"/>
               </Switch>
           </NativeRouter>
        )
    }
}
export default Router
