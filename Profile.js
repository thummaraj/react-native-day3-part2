import React, {Component} from 'react'
import {View,Text, StyleSheet, TouchableOpacity} from 'react-native'



class Profile extends Component {
    UNSAFE_componentWillMount(){
        console.log(this.props)
        // if(this.props.location && this.props.location.state && this.props.location.state.myusername){
        //     Alert.alert('Your username is', this.props.location.state.myusername + '')
        // }
    }
    onBackToList=()=>{
        this.props.history.push('/list')
    }
    goToEditProfile=()=> {
        this.props.history.push('/editprofile')
    }
    render() {
        return (
            <View style={{flex:1, backgroundColor: 'white'}}>
             <View style={styles.column}>
             <TouchableOpacity onPress={this.onBackToList}>
             <View style={styles.footer1}>
            <Text style={styles.footerText}>{`<`}</Text>
            </View>
            </TouchableOpacity>
            <View style={styles.footer2}>
            <Text style={styles.footerText}>My Profile</Text>
            </View>
             </View>

             <View style={styles.content}>
            <Text>Username</Text>
            <Text>Fistname</Text>
            <Text>Lastname</Text>
            </View>


             <View style={styles.column}>
             <TouchableOpacity style={styles.footer2} onPress={this.goToEditProfile}>
            <Text style={styles.footerText}>edit</Text>
            </TouchableOpacity>
            </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    column: {
        flexDirection: 'row',
        
    },
    footer1: {
        flex:1,
        borderColor: 'white',
        borderWidth: 2,
        backgroundColor: 'pink',
        padding: 20
        
    },
    content: {
        flex: 3
    },
    footer2: {
        flex:1,
        borderColor: 'white',
        borderWidth: 2,
        alignItems: 'center',
        padding: 16,
        backgroundColor: 'pink'
       
    },
    footerText: {
        fontSize: 22,
        color: 'white',
    }
})
export default Profile
