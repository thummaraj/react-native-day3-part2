import React, {Component} from 'react'
import {View,Text, Button, TextInput, StyleSheet, Image,Alert} from 'react-native'

class Login extends Component {
    state = {
        username: '',
        password: ''
    }

    goToProfile=()=> {
        if( this.state.username.toLowerCase() === 'jaja' && this.state.password.toLowerCase() === '1234'){
            this.props.history.push('/list', {
                myusername: this.state.username
            })
        } else{
            Alert.alert('Username or Password is not correct')
        }
        
    }

    render() {
        return (
            <View style={{flex:1, backgroundColor: 'white'}}>
           <View style={[styles.layer1, styles.center]}>
            <View style={[styles.circle, styles.center]}>
            <Image source={require('./cat1.jpg')} style={[styles.circle, styles.center]} />
            </View>
            </View>
            
            <View  style={styles.layer2}>
                <Text style={{ alignItems: 'center'}}>username: {this.state.username} - password: {this.state.password}</Text>
            <TextInput onChangeText={ (username) => {this.setState({username})}}  
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white', margin: 14 }}></TextInput>
            <TextInput onChangeText={ (value) => {this.setState({password: value})}}  
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white', margin: 14 }}></TextInput>
            <Button title="Login" onPress={this.goToProfile}/>
            </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    circle: {
        width: 200,
        height: 200,
        borderRadius: 200/2,
        backgroundColor: 'gray',
    },
    layer1: {
        
        flex: 1,
        
        
    },
    layer2: {
        
        flex: 1,
        
    },
    rectangle: {
        backgroundColor: 'white',
        padding: 20,
        margin: 14
    
    },
    rectangle1: {
        backgroundColor: 'black',
        padding: 20,
        margin: 14
    
    }

})

export default Login