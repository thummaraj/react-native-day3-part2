import React, {Component} from 'react'
import {View,Text,StyleSheet,TouchableOpacity} from 'react-native'

class AddProduct extends Component{
    onBackToList=()=>{
        this.props.history.push('/list')
    }
    render(){
        return(
            <View style={{flex:1, backgroundColor: 'white'}}>
               <View style={styles.column}>
             <TouchableOpacity onPress={this.onBackToList}>
             <View style={styles.footer1}>
            <Text style={styles.footerText}>{`<`}</Text>
            </View>
            </TouchableOpacity>
            <View style={styles.footer2}>
            <Text style={styles.footerText}>Add Product</Text>
            </View>
             </View>

             <View style={styles.content}>

             </View>

              <View style={styles.column}>
             <TouchableOpacity style={styles.footer2}>
            <Text style={styles.footerText}>save</Text>
            </TouchableOpacity>
            </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    column: {
        flexDirection: 'row',
        
    },
    footer1: {
        flex:1,
        borderColor: 'white',
        borderWidth: 2,
        backgroundColor: 'pink',
        padding: 20
        
    },
    content: {
        flex: 3
    },
    footer2: {
        flex:1,
        borderColor: 'white',
        borderWidth: 2,
        alignItems: 'center',
        padding: 16,
        backgroundColor: 'pink'
       
    },
    footerText: {
        fontSize: 22,
        color: 'white',
    }
})
export default AddProduct