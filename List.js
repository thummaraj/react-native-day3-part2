import React, { Component } from "react";
import {
  Button,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  ScrollView
} from "react-native";

class List extends Component {
    state = {
        isShowModal: false,
        images: ""
      };
    
      showModal(visible) {
        this.setState({ isShowModal: visible });
      }
      showImage(image) {
        this.setState({ images: image });
      }
      goTomyProfile=()=> {
        this.props.history.push('/profile',{
            myusername: this.state.username
        })
      }
      goToAddProduct=()=>{
        this.props.history.push('/addproduct')
      }
      goToProduct=()=>{
        this.props.history.push('/product')
      }
    render() {
        return (
            <View style={styles.containers}>
            <View style={styles.column}>
            <View style={styles.footer}>
            <Text style={styles.footerText}>List of cats</Text>
            </View>
            </View>

           
            
            
            <ScrollView>
          <View style={styles.content}>
            <View style={styles.row}>
              <TouchableOpacity
                onPress={() => {
                  this.showModal(true);
                  this.showImage( require("./cat7.jpg"));
                }}
              >
                <View style={styles.box1}>
                  <Image
                    source={require("./cat7.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat1</Text>
                </View>
              </TouchableOpacity>

              <Modal  
      transparent={true}
      visible={this.state.isShowModal}>
      
      <View style={[styles.modal]}>
     <Button color="#e60000" title="Hide Modal" onPress={() => { this.showModal(!this.state.isShowModal)}}></Button>
     <Image source={this.state.images} style={styles.imgModal} resizeMode="stretch"></Image>
     </View>

      </Modal>

              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage(require( "./cat2.jpg"));
                }}>
                <View style={styles.box2}>
                  <Image
                    source={require("./cat2.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat2</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={styles.row}>
              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage( require("./cat3.jpg"));
                }}>
                <View style={styles.box1}>
                  <Image
                    source={require("./cat3.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat3</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage(require("./cat4.jpg"));
                }}>
                <View style={styles.box2}>
                  <Image
                    source={require("./cat4.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat4</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={styles.row}>
              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage( require("./cat5.jpg"));
                }}>
                <View style={styles.box1}>
                  <Image
                    source={require("./cat5.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat5</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage(require("./cat6.jpg") );
                }}>
                <View style={styles.box2}>
                  <Image
                    source={require("./cat6.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat6</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={styles.row}>
              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage( require("./cat8.jpg"));
                }}>
                <View style={styles.box1}>
                  <Image
                    source={require("./cat8.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat7</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage(require("./cat9.jpg"));
                }}>
                <View style={styles.box2}>
                  <Image
                    source={require("./cat9.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat8</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
            
           


            <View style={styles.column}>
            <TouchableOpacity style={styles.footer} onPress={this.goToProduct}>
            <Text style={styles.footerText}>L</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.footer2} onPress={this.goToAddProduct}>
            <Text style={styles.footerText}>Add</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.footer} onPress={this.goTomyProfile}>
            <Text style={styles.footerText}>P</Text>
            </TouchableOpacity>
            </View>
            


           </View>
        )
    }
}
const styles = StyleSheet.create({
    containers: {
        backgroundColor: 'white',
        flex:1
    },
    column: {
        flexDirection: 'row'
    },
    header: {
        backgroundColor: 'pink',
        flex: 1
    },
    footer: {
        backgroundColor: 'pink',
        flex: 1,
        padding: 16,
        margin: 2,
        alignItems: 'center'
        
    },
    footer2: {
        backgroundColor: 'pink',
        flex: 2,
        padding: 16,
        margin: 2,
        alignItems: 'center'
    },
    footerText: {
        fontSize: 22,
        color: 'white',
        alignItems: 'center'
    },
    textContent: {
        fontSize: 22,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modal: {
        marginLeft: 30,
        marginTop: 30,
        width: '80%',
        height: '80%',
        backgroundColor: 'white',
    },
    imgModal: {
      width: '100%',
      height: '100%',
        padding: 50
    },
    content: {
        flex: 1,
        flexDirection: "column",
        
      },
    
      box1: {
        flex: 1,
        margin: 14,
        height: 200
      },
      box2: {
        flex: 1,
        margin: 14,
        height: 200
      },
      row: {
        backgroundColor: "white",
        flex: 1,
        flexDirection: "row"
      },
      box: {
        width: 150,
        height: 120
      },
  
})
export default List